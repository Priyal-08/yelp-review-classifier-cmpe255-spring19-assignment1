import file_extractor
import json
import warnings
FILE_CLASSIFIED_REVIEWS = './Dataset/classified_reviews.json'
FILE_RESTAURANTS = './Dataset/restaurants.json'
FILE_FINAL_RESULTS = './Output/final_results.txt'
FILE_SIMILAR_RESULTS = './Output/similar_results.txt'
FILE_OPPOSITE_RESULTS = './Output/different_results.txt'
OBSERVATION_COUNT = 50


def load_data():
    print("Loading classified reviews...\n")
    data = []
    data_labels = []
    data_id = []

    with open(FILE_CLASSIFIED_REVIEWS) as f:
        for i in f:
            review = json.loads(i)
            data.append(review['text'])
            data_labels.append(review['label'])
            data_id.append(review['business_id'])

    return data, data_labels, data_id


def transform_to_features(data):
    print("Transforming reviews to features...\n")
    from sklearn.feature_extraction.text import CountVectorizer
    vectorizer = CountVectorizer(
        analyzer='word',
        lowercase=False,
    )
    features = vectorizer.fit_transform(
        data
    )
    features_nd = features.toarray()
    return features_nd


def train_then_build_model(data_labels, features_nd, data, data_id):
    print("Training and Building model...\n")
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(
        features_nd,
        data_labels,
        train_size=0.80,
        random_state=1234)

    from sklearn.linear_model import LogisticRegression
    log_model = LogisticRegression()

    log_model = log_model.fit(X=X_train, y=y_train)
    y_pred = log_model.predict(X_test)

    rest_review = {}
    print("Consolidating the predicted results restaurant-wise...\n")
    feature_list = features_nd.tolist()
    for i in range(len(X_test)):
        ind = feature_list.index(X_test[i].tolist())
        if data_id[ind] not in rest_review.keys():
            rest_review[data_id[ind]] = {
                'Positive': 0, 'Negative': 0, 'Neutral': 0}
        if(y_pred[i] == 'Positive'):
            rest_review[data_id[ind]]['Positive'] += 1
        elif(y_pred[i] == 'Negative'):
            rest_review[data_id[ind]]['Negative'] += 1
        else:
            rest_review[data_id[ind]]['Neutral'] += 1

    print("Calculating final class...\n")
    average_review = {}
    for key in rest_review.keys():
        pos = rest_review[key]['Positive']
        neg = rest_review[key]['Negative']
        neu = rest_review[key]['Neutral']
        if(pos > neg and pos > neu):
            average_review[key] = 'Positive'
        elif (neg > neu):
            average_review[key] = 'Negative'
        else:
            average_review[key] = 'Neutral'

    rest_review.clear()

    yelp_review = {}
    with open(FILE_RESTAURANTS) as rest:
        for i in rest:
            yelp_r = json.loads(i)
            yelp_review[yelp_r['business_id']] = yelp_r['stars']

    print("Creating final comparison files...\n")
    similar_results_file = open(FILE_SIMILAR_RESULTS, 'w')
    opposite_results_file = open(FILE_OPPOSITE_RESULTS, 'w')
    similar_count = 0
    opposite_count = 0
    similar_results_file.write(
        "{:<25}  {:<15}  {:<3}".format('BUSINESS ID', 'CALC RESULT', 'YELP RATING'))
    similar_results_file.write('\n')
    opposite_results_file.write(
        "{:<25}  {:<15}  {:<3}".format('BUSINESS ID', 'CALC RESULT', 'YELP RATING'))
    opposite_results_file.write('\n')
    with open(FILE_FINAL_RESULTS, 'w') as f:
        f.write(
            "{:<25}  {:<15}  {:<3}".format('BUSINESS ID', 'CALC RESULT', 'YELP RATING'))
        f.write('\n')
        for key in average_review.keys():
            id = key
            calc_rating = average_review[key]
            yelp_rating = yelp_review[key]
            f.write("{:<25}  {:<15}  {:<3}".format(
                id, calc_rating, yelp_rating))
            f.write('\n')
            if(yelp_rating >= 5):
                if(calc_rating == 'Positive' and similar_count < OBSERVATION_COUNT):
                    similar_count += 1
                    similar_results_file.write(
                        "{:<25}  {:<15}  {:<3}".format(id, calc_rating, yelp_rating))
                    similar_results_file.write('\n')
                elif(calc_rating == 'Negative' and opposite_count < OBSERVATION_COUNT):
                    opposite_count += 1
                    opposite_results_file.write(
                        "{:<25}  {:<15}  {:<3}".format(id, calc_rating, yelp_rating))
                    opposite_results_file.write('\n')
            elif(yelp_rating < 2):
                if(calc_rating == 'Negative' and similar_count < OBSERVATION_COUNT):
                    similar_count += 1
                    similar_results_file.write(
                        "{:<25}  {:<15}  {:<3}".format(id, calc_rating, yelp_rating))
                    similar_results_file.write('\n')
                elif(calc_rating == 'Positive' and opposite_count < OBSERVATION_COUNT):
                    opposite_count += 1
                    opposite_results_file.write(
                        "{:<25}  {:<15}  {:<3}".format(id, calc_rating, yelp_rating))
                    opposite_results_file.write('\n')

    f.close()

    print("Analyzing Results...\n")
    from sklearn.metrics import accuracy_score, confusion_matrix
    import numpy as np
    accuracy = accuracy_score(y_test, y_pred)
    cm = confusion_matrix(y_test, y_pred)
    recall = np.diag(cm) / np.sum(cm, axis=1)
    precision = np.diag(cm) / np.sum(cm, axis=0)
    recall = np.mean(recall)
    precision = np.mean(precision)
    f1score = (2*precision*recall)/(precision+recall)
    print("Accuracy= {}".format(accuracy))
    print("Precision= {}".format(precision))
    print("Recall= {}".format(recall))
    print("F1-score= {}".format(f1score))

    from sklearn.model_selection import cross_val_score
    print("Cross Validation Score= {}\n".format(cross_val_score(
        log_model, X_train, y_train, cv=3, scoring="accuracy")))


def process():
    warnings.filterwarnings("ignore", category=FutureWarning)
    print("Starting analysis...\n")
    file_extractor.extract_reviews()
    data, data_labels, data_id = load_data()
    features_nd = transform_to_features(data)
    train_then_build_model(data_labels, features_nd, data, data_id)


process()
