
from sklearn.feature_extraction.text import CountVectorizer
import json
FILE_CLASSIFIED_REVIEWS = './Dataset/classified_reviews.json'
FILE_POSITIVE_TEXT = './Dataset/positive_words.txt'
FILE_NEGATIVE_TEXT = './Dataset/negative_words.txt'


def classify(filtered_reviews_file):
    pos_words = []
    with open(FILE_POSITIVE_TEXT) as f:
        for line in f:
            pos_words.append(line.replace("\n", ""))
    f.close()

    neg_words = []
    with open(FILE_NEGATIVE_TEXT) as f:
        for line in f:
            neg_words.append(line.replace("\n", ""))
    f.close()

    classified_review_file = open(FILE_CLASSIFIED_REVIEWS, 'w')
    with open(filtered_reviews_file) as f:
        for line in f:
            try:
                review = {}
                review = json.loads(line)
                vectorizer = CountVectorizer(stop_words="english")
                vectorizer.fit_transform([review['text']])
                words = vectorizer.get_feature_names()
                pos_count = sum(count in words for count in pos_words)
                neg_count = sum(count in words for count in neg_words)
                if pos_count - neg_count < -1:
                    review["label"] = "Negative"
                elif pos_count - neg_count > 1:
                    review["label"] = "Positive"
                else:
                    review["label"] = "Neutral"
                classified_review_file.write(json.dumps(review))
                classified_review_file.write("\n")
            except ValueError:
                # If the current review contains only stop words then continue to the next one
                continue

    f.close()
    classified_review_file.close()
