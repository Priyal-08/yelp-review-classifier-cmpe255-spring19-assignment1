import json
import operator
import review_classifier as review_classifier
FILE_RESTAURANTS = './Dataset/restaurants.json'
FILE_BUSINESS = './Dataset/business.json'
FILE_REVIEWS = './Dataset/review.json'
FILE_FILTERED_REVIEWS = './Dataset/filtered_reviews.json'
REVIEW_COUNT = 5000


def filter_restaurants():
    print("Extracting restaurants from business file...\n")
    restaurants_file = open(FILE_RESTAURANTS, 'w')
    with open(FILE_BUSINESS) as f:
        for business in f:
            business_data = json.loads(business)
            business_id = business_data['business_id']
            categories = business_data['categories']
            if categories and 'Restaurants' in categories:
                restaurants_file.write(json.dumps(
                    {'business_id': business_id, 'stars': business_data['stars']}))
                restaurants_file.write('\n')
    f.close()
    restaurants_file.close()


def extract_reviews():
    filter_restaurants()
    print("Extracting reviews from review file...\n")

    restaurants = []
    with open(FILE_RESTAURANTS) as f:
        for i in f:
            rest_data = json.loads(i)
            restaurants.append(rest_data['business_id'])
    f.close()

    filtered_reviews_file = open(FILE_FILTERED_REVIEWS, 'w')
    with open(FILE_REVIEWS) as raw_reviews_file:
        good_count = 0
        bad_count = 0
        neutral_count = 0
        for review in raw_reviews_file:
            if(good_count >= REVIEW_COUNT and bad_count >= REVIEW_COUNT and neutral_count >= REVIEW_COUNT):
                break
            data = json.loads(review)
            if data['business_id'] not in restaurants:
                continue
            stars = data['stars']
            curr_review = {
                'business_id': data['business_id'], 'text': (data['text']).replace('\n', ' ').replace('\r', '').replace('\\', '').strip()}
            filtered_reviews_file.write(json.dumps(curr_review))
            filtered_reviews_file.write('\n')
            if stars < 2.5 and bad_count < REVIEW_COUNT:
                bad_count += 1
            elif (stars >= 2.5 and stars <= 3.5 and neutral_count < REVIEW_COUNT):
                neutral_count += 1
            elif(stars > 3.5 and good_count < REVIEW_COUNT):
                good_count += 1

    raw_reviews_file.close()
    filtered_reviews_file.close()
    sort_reviews(FILE_FILTERED_REVIEWS)
    classify_reviews(FILE_FILTERED_REVIEWS)


def sort_reviews(review_file):
    print("Sorting the extracted reviews...\n")
    reviews = []
    with open(review_file) as f:
        for line in f:
            reviews.append(json.loads(line))
    f.close()
    reviews.sort(key=operator.itemgetter('business_id'))
    with open(review_file, 'w') as f:
        for r in reviews:
            f.write(json.dumps(r))
            f.write('\n')
    f.close()


def classify_reviews(sorted_review_file):
    print("Classifying reviews to positive, negative and neutral...\n")
    review_classifier.classify(sorted_review_file)
