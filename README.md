# Yelp Review Classifier

Classifies yelp restaurant's reviews to following:

- Neutral (reviews with keywords like fine, OK etc..)
- Positive (reviews with keywords like great, awesome etc..)
- Negative (reviews with keywords like bad, awful etc..)

## Requirements
- Python3
- Scikit Learn library


## Configuration
1. Number of records to be considered for comparison is configurable.
	- By default it's set to 5000 which means it will consider 15000 (5000 positive, 5000 negative and 5000 neutral) records for comparison.
	- It can be changed by changing the value of 'REVIEW_COUNT' in file_extractor.py.

2. Max number of similar and opposite instances to be flagged is configurable.
	- By default it's set to 50 which means it will only record first 50 similar and opposite instances in [similar output file](https://bitbucket.org/Priyal-08/cmpe255-spring19-assignment1/raw/master/Output/similar_results.txt) and [opposite output file](https://bitbucket.org/Priyal-08/cmpe255-spring19-assignment1/raw/master/Output/different_results.txt).
	- It can be changed by changing the value of 'OBSERVATION_COUNT' in yelp_analysis.py.
## How to run
1. Download [Yelp Open Dataset](https://www.yelp.com/dataset).
2. Place the review.json and business.json files in Dataset folder.
3. Run the application on terminal as follows:
	
	python3 yelp_analysis.py

## Output

![](https://bitbucket.org/Priyal-08/cmpe255-spring19-assignment1/raw/master/Output/output.png)


- Accuracy= 0.83935475269964
- Precision= 0.7331006460903003
- Recall= 0.6701790123121677
- F1-score= 0.7002291581444695
- Cross Validation Score= [0.82931707 0.8292     0.82648265]


## Comparison

![](https://bitbucket.org/Priyal-08/cmpe255-spring19-assignment1/raw/master/Output/final%20comparison.png)

## Similar Instances

![](https://bitbucket.org/Priyal-08/cmpe255-spring19-assignment1/raw/master/Output/similar.png)

## Opposite Instances

![](https://bitbucket.org/Priyal-08/cmpe255-spring19-assignment1/raw/master/Output/opposite.png)

Complete results can be found [here](https://bitbucket.org/Priyal-08/cmpe255-spring19-assignment1/raw/master/Output/final_results.txt).
